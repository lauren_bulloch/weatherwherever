import { WeatherService } from './../services/weather.service';
import { Component, ViewChild, NgZone, OnInit } from '@angular/core';
import { MapsAPILoader, AgmMap, LatLngLiteral } from '@agm/core';
import { GoogleMapsAPIWrapper } from '@agm/core/services';
import { FormControl, Validators } from '@angular/forms';
declare var google: any;
declare var require: any;

interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})

export class MapComponent implements OnInit {
  geocoder: any;
  Math: any;
  public weather_response = {};
  public latitude: number;
  public longitude: number;
  public marker: Marker;
  public styles: any;
  public address_level_1: any;
  public full_address: any;
  public address_level_2: any;
  public address_state: any;
  public address_country: any;
  public address_zip: any;
  public formatted_address: any;
  public temp: any;
  public pressure: any;
  public humidity: any;
  public temp_min: any;
  public temp_max: any;
  public main: any;
  public map_loaded = false;
  public map_changed = false;

  @ViewChild(AgmMap) map: AgmMap;

  constructor(
    public mapsApiLoader: MapsAPILoader,
    private zone: NgZone,
    private wrapper: GoogleMapsAPIWrapper,
    private weather: WeatherService) {
    this.mapsApiLoader = mapsApiLoader;
    this.zone = zone;
    this.wrapper = wrapper;
    this.Math = Math;
    this.mapsApiLoader.load().then(() => {
      this.geocoder = new google.maps.Geocoder();
    });
  }

  country = new FormControl('', [Validators.required]);

  getErrorMessage() {
    return this.country.hasError('required') ? 'You must enter a country' : '';
  }

  ngOnInit() {
    this.weather.getJSON().subscribe(data => {
      this.styles = data;
    });
    this.latitude = 18.51;
    this.longitude = -33.89;
    this.setCurrentPosition();
  }

  spinnerTime() {
  setTimeout(() => { this.map_loaded = true; }, 2000);
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        const latlng: LatLngLiteral = {
          lat: this.latitude,
          lng: this.longitude
        };

        this.weather.getResponse(this.latitude, this.longitude)
          .subscribe((response: any) => {
            this.temp = this.Math.round(response.main.temp - 273);
            this.pressure = response.main.pressure;
            this.humidity = response.main.humidity;
            this.temp_min = this.Math.round(response.main.temp_min - 273);
            this.temp_max = this.Math.round(response.main.temp_max - 273);
          }
          );
      });
    }
  }

  markerIconUrl() {
    if (this.temp < 20) {
      return require('../../assets/img/cold-marker.png');
    } else {
      return require('../../assets/img/hot-marker.png');
    }
  }

  updateOnMap() {
    this.map_loaded = false;

    let full_address: string = this.address_level_1 || '';

    if (this.address_level_2) {
      full_address = full_address + ' ' + this.address_level_2;
    }

    if (this.address_state) {
      full_address = full_address + ' ' + this.address_state;
    }

    if (this.address_country) {
      full_address = full_address + ' ' + this.address_country;
    }
    this.findLocation(full_address);
  }

  findLocation(address) {
    if (!this.geocoder) {
      this.geocoder = new google.maps.Geocoder();
    }
    this.geocoder.geocode({
      'address': address
    }, (results, status) => {
      this.formatted_address = results[0].formatted_address;
      this.address_level_2 = results[0].address_level_2;
      if (status === google.maps.GeocoderStatus.OK) {
        for (let i = 0; i < results[0].address_components.length; i++) {
          const types = results[0].address_components[i].types;

          if (types.indexOf('locality') !== -1) {
            this.address_level_2 = results[0].address_components[i].long_name;
          }
          if (types.indexOf('country') !== -1) {
            this.address_country = results[0].address_components[i].long_name;
          }
          if (types.indexOf('postal_code') !== -1) {
            this.address_zip = results[0].address_components[i].long_name;
          }
          if (types.indexOf('administrative_area_level_1') !== -1) {
            this.address_state = results[0].address_components[i].long_name;
          }
        }

        if (results[0].geometry.location) {
          this.latitude = results[0].geometry.location.lat();
          this.longitude = results[0].geometry.location.lng();
        }
      } else {
        alert('Sorry, this search produced no results.');
      }
      this.zone.run(() => {
        this.latitude = results[0].geometry.location.lat();
        this.longitude = results[0].geometry.location.lng();
        this.weather.getResponse(this.latitude, this.longitude)
          .subscribe((response: any) => {
            this.temp = this.Math.round(response.main.temp - 273);
            this.pressure = response.main.pressure;
            this.humidity = response.main.humidity;
            this.temp_min = this.Math.round(response.main.temp_min - 273);
            this.temp_max = this.Math.round(response.main.temp_max - 273);
          });
          setTimeout(() => { this.map_loaded = true; }, 2000);
        });
    });
  }
}
