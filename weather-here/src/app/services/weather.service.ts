import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'rxjs/Rx';
import { Injectable } from '@angular/core';

@Injectable()
export class WeatherService {

  constructor(private http: HttpClient) {
    this.getJSON().subscribe(data => {});
  }
  getResponse(lat, lng) {
    const params = new HttpParams().set('lat', lat).set('lon', lng).set('appid', '6e834d60bdc058b7129dcf871c70c8f3');
    return this.http.get('https://api.openweathermap.org/data/2.5/weather', { params })
      .map((response: Response) => {
        return response;
      })
      .catch((error: Response) => {
        return Observable.throw('there is an error');
      });
  }
  public getJSON(): Observable<any> {
    return this.http.get('../../assets/mapStyle.json');
  }
}
